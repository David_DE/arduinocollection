#ArduinoCollection

## Introduction

Welcome to my Arduino collection repository!

I'm completely new to Arduino and fairly new to the whole microcontroller, single-board
computer world. I started getting into it with the release of the raspberry pi
and because it is a very interesting area I'm trying out different kinds of
devices.

Here I'm going to try to document my learning progress. This means that I will
not only post the source code of every project but also try to write a small
documentation for each one.

## My Goal

My main goal and the reason why I want to learn "Arduino" is to control things via
smart phone apps (mainly android right now).

**My steps to get there:**

1. Learn the basics of Arduino
2. Learn how to use the Ethernet Shield
  * set up a (web) server
3. Learn how to use Bluetooth on the Arduino
  * to comunicate with Android apps
  * as well as with other Arduinos
4. ???

## Projects

To find more information about the projects look in the corrensponding readme.

1. Basics
  1. Distance Measurement
2. Ethernet
3. Bluetooth
