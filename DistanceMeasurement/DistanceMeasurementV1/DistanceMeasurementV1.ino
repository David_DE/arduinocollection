/*
  Distance Mesurment V1

  This code will mesure the distance between the HC-SR04 and
  an object infront of it.

 */

int led = 13;
int TRIGGER = 7;
int ECHO = 8;

unsigned long time;


float distance();

// the setup routine runs once when you press reset:
void setup() {

  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);

  pinMode(led, OUTPUT);
  pinMode(TRIGGER, OUTPUT);

  pinMode(ECHO, INPUT);

}

// the loop routine runs over and over again forever:
void loop() {
  Serial.println("Distance Mesurment");

  digitalWrite(led, HIGH);

  float dis = distance();

  //Max Range 4m; Min Range 2cm
  if (dis < 400) {
    Serial.println(dis);
  } else {
   Serial.println("Error: Can't mesure more than 4m!");
  }

  digitalWrite(led, LOW);
  delay(2000);

}

/**
* distance() calculates the distance in cm:
*/
float distance() {

  /*
   * The ultrasonic module will start to mesure the distance
   * after a 10 microsecond pulse.
   */
  digitalWrite(TRIGGER, LOW);
  delayMicroseconds(5);
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER, LOW);

  /*
   * pulseIn returns the length of the pulse. (Time from high to low)
   * This is the time the ultrasonic waves took to reach an object and
   * to return to the module.
   */
  time = pulseIn(ECHO, HIGH);

  /*
   * /29 - 1 microseconds means 29 cm
   * /02 - The sonic waves needed to go to the object and back
   */
  return time / (float)29 / 2;

}
