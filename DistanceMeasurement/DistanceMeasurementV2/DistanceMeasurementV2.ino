/*
  Distance Mesurment V2
 
 This programm uses the ultra sonic module (HC-SR04) to detect movement.
 
 */

int OUTPUT_LED_STATUS = 12;
int OUTPUT_LED_ALARM = 13;

int OUTPUT_TRIGGER = 7;
int INPUT_ECHO = 8;

int INPUT_BUTTON_RESET = 4;

unsigned long time;

int CURRENT_MODE;

//SETUP: Waiting 10 seconds of no movement
const int MODE_SETUP = 0;

//ARMED: The alarm system is armed and waiting for movement
const int MODE_ARMED = 1;

//ALARM: The alarm system has been set off and needs to be reset.
const int MODE_ALARM = 2;

//The time the loop waits while in setup and alarm mode
const int DELAY_STANDARD = 500;

//The time the loop waits in armed mode.
const int DELAY_ARMED = 150;

int CURRENT_DELAY = DELAY_STANDARD;

float savedDistance;
float savedDistanceLast;
unsigned long savedTime;
unsigned long savedTimeLast;


float DISTANCE_TOLERANCE = 0.8;
unsigned long TIME_TOLERANCE = 10000; //10s

float distance();

// the setup routine runs once when you press reset:
void setup() {

  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);

  pinMode(OUTPUT_LED_ALARM, OUTPUT);
  pinMode(OUTPUT_LED_STATUS, OUTPUT);

  pinMode(OUTPUT_TRIGGER, OUTPUT);
  pinMode(INPUT_ECHO, INPUT);

  pinMode(INPUT_BUTTON_RESET, INPUT);

  CURRENT_MODE = MODE_SETUP;

  savedTimeLast = millis();
  savedDistanceLast = distance();

  digitalWrite(OUTPUT_LED_ALARM, LOW);
  digitalWrite(OUTPUT_LED_STATUS, LOW);

  Serial.println("Alarm System - Setup complete");
}

// the loop routine runs over and over again forever:
void loop() {

  switch(CURRENT_MODE) {
  case MODE_SETUP: 
    {
      Serial.println("Mode: Setup");
      digitalWrite(OUTPUT_LED_ALARM, LOW);
      digitalWrite(OUTPUT_LED_STATUS, HIGH);

      /*
       * In SETUP mode the program checks the time and the current measured distance.
       * To "arm" the alarm system the distance must not change more than the 
       * tolerance for 10 seconds.
       */

      if (savedDistanceLast > 400) {
        Serial.println("Distance needs to be smaller than 400cm! Reseting distance now");
        savedTimeLast = millis();
        savedDistanceLast = distance();
        break;
      }

      savedTime = millis();
      savedDistance = distance();

      float tempDistance = savedDistance - savedDistanceLast;

      if (abs(tempDistance) < DISTANCE_TOLERANCE) {
        Serial.println("Distance has not changed!");

        unsigned long tempTime = savedTime - savedTimeLast;

        Serial.println("Time:");
        Serial.println(tempTime);

        if (tempTime > TIME_TOLERANCE) {
          //save distance + 10s waited -> armed
          Serial.println("Time > 10s; System armed!");
          CURRENT_MODE = MODE_ARMED;
        }


      } 
      else {
        Serial.println("Distance needs to be stable for 10s to arm the system.");
        savedTimeLast = millis();
        savedDistanceLast = distance();
      }

      delay(100);
      digitalWrite(OUTPUT_LED_STATUS, LOW);

      break;
    }
  case MODE_ARMED:
    {
      Serial.println("Mode: Armed");
      digitalWrite(OUTPUT_LED_ALARM, HIGH);
      CURRENT_DELAY = DELAY_ARMED;

      /*
       * After the alarm system is armed. A change in the distance will set the system off and switches it to the alarm mode
       */
      savedDistance = distance();
      float tempDistance = savedDistance - savedDistanceLast;

      if (abs(tempDistance) > DISTANCE_TOLERANCE) {
        Serial.println("ALARM! Movement detected!");
        CURRENT_MODE = MODE_ALARM;
        break;
      }

      delay(100);
      digitalWrite(OUTPUT_LED_ALARM, LOW);
      break;
    }
  case MODE_ALARM:
    {
      Serial.println("Mode: Alarm");
      /*
       * Alarm has been set off. The button resets the alarm and switches the mode back to SETUP
       */
      CURRENT_DELAY = DELAY_STANDARD;
      digitalWrite(OUTPUT_LED_ALARM, HIGH);

      int value = digitalRead(INPUT_BUTTON_RESET);

      if (value == HIGH) {
        Serial.println("Alarm reset!");
        CURRENT_MODE = MODE_SETUP;

        savedTimeLast = millis();
        savedDistanceLast = distance();
        digitalWrite(OUTPUT_LED_ALARM, LOW);
      }

      break;
    }
  default: 
    Serial.println("Wrong mode");
    break;
  }

  delay(CURRENT_DELAY);

}

/**
 * distance() calculates the distance in cm:
 */
float distance() {

  /*
   * The ultrasonic module will start to mesure the distance
   * after a 10 microsecond pulse.
   */
  digitalWrite(OUTPUT_TRIGGER, LOW);
  delayMicroseconds(5);
  digitalWrite(OUTPUT_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(OUTPUT_TRIGGER, LOW);

  /*
   * pulseIn returns the length of the pulse. (Time from high to low)
   * This is the time the ultrasonic waves took to reach an object and
   * to return to the module.
   */
  time = pulseIn(INPUT_ECHO, HIGH);

  /*
   * /29 - 1 microseconds means 29 cm
   * /02 - The sonic waves needed to go to the object and back
   */
  return time / (float)29 / 2;

}




