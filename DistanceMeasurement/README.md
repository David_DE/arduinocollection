# DistanceMeasurement

This is my first Arduino project. I had the ultra sonic module (HC-SR04)
lying around for some time and thought this would be a nice way to use it.

The project itself is nothing fancy. Just an Arduino Uno and an ultra sonic
ranging module. The LEDs are not needed.

To measure distances with the HC-SR04 you have to give it a 10μs long trigger
impulse. Then the module starts sending out eight 40kHz signals and detectes
whether they return. If they return you can calculate the distance by measuring
the length of the high echo signal, multiplied by the speed of sound and
divided by two because the signal travels twice the distance.

## Version 1 - The basics

To get started I just used serial communication to print the measured distances
to the screen.

### Hardware

#### Parts
* Arduino Uno
* HC-SR04
* Wires, leds, resistors

#### Wiring

| Arduino       | HC-SR04     | Comment    |
| ------------: |:-----------:| :----------|
| GND           | GND         | Ground     |
| 5V            | VCC         | Current    |
| 7 (digital)   | TRIG        | Output     |
| 8 (digital)   | ECHO        | Input      |

![Wiring](https://raw.githubusercontent.com/David-GER/ArduinoCollection/master/DistanceMeasurement/img/version1_hc_sr04.png)



### Software

A list of functions and libraries (if any) I have used in this
project (for the first time).

* [delayMicroseconds()](http://arduino.cc/de/Reference/DelayMicroseconds):
To start the measurement the HC needs a 10μs impulse.
* [pulseIn()](http://arduino.cc/de/Reference/PulseIn):
To easily mesure the length of a pulse.
* [digitalWrite()](http://arduino.cc/de/Reference/DigitalWrite):
To start/stop the impulse
* [Serial.println();](http://arduino.cc/de/Reference/Serial):
To print the result on the screen.

### Pictures

![My setup](https://raw.githubusercontent.com/David-GER/ArduinoCollection/master/DistanceMeasurement/img/setup_v1.png)

##Version 2 - An "alarm system"

This program uses the distance to easily detect movement.
This is obviously not the best way to do it, but it works more or less and is an interesting concept.

The program is divided into three phases or modes:

1. Setup mode:
  * Duration of 10 seconds
  * If the currentDistance != the last distance
     * reset
  * If the currentTime is 10 seconds after the lastTime
     * Switch to armed mode
2. Armed mode:
  * If current distance != last distance
     * switch to alarm mode
3. Alarm mode:
  * If the reset button is pressed
    * Go back to setup mode

### Hardware

#### Parts
* Arduino Uno
* HC-SR04
* Wires, leds, resistors
* Button

#### Wiring

The wiring of the HC-SR04 is identical to Version 1.

![Wiring](https://raw.githubusercontent.com/David-GER/ArduinoCollection/master/DistanceMeasurement/img/version2_leds_button.png)

The LEDs are optional.


### Software

A list of functions and libraries (if any) I have used in this
project (for the first time).

* [digitalRead()](http://arduino.cc/en/Reference/DigitalRead):
To read the HIGH signal via a button.
* [millis()](http://arduino.cc/en/Reference/Millis):
To determine the time  

##Version 3 - A display (TODO)

I want to hook up a small 20x2 character screen to the Arduino to display
the measurements. (Based on Version 1)

##Version 4 - Alarm system with sound (TODO)

Like a real alarm system the alarm mode should not only enable a LED but also
something to generate sound. (Based on Version 2)
